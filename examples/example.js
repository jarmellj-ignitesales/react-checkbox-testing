import React, {Component} from 'react';
import { render } from 'react-dom';
import { debounce } from 'debounce';
import {Checkbox, CheckboxGroup} from "../src";
import '../src/styles/checkbox.scss';
import 'bootstrap/scss/bootstrap.scss';

class App extends Component {
    saveAnswer = debounce(
        (value) => {
            console.log('Checkbox answers values saved:', value);
        },
        500
    );

    render() {
        return (
            <CheckboxGroup
                name={"testing"}
                checkboxDepth={2}
                className={"row"}
                onChange={this.saveAnswer}
                value={[1]}
            >
                <label className={"col-3"}> <span>Hello</span><Checkbox value={'Hello'}/> </label>
                <label className={"col-3"}> <span>World</span><Checkbox value={'World'}/> </label>
            </CheckboxGroup>
        );
    }
}

render(<App />, document.getElementById("app"));