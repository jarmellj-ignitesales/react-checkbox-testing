# React Checkbox Component

> Checkbox component for Ignite Sales

Development Instructions:
- Download Code
- Run "npm install"
- To see example, run "npm run start" 

## License
MIT License. Copyright &copy; 2019 [Ignite Sales](http://ignitesales.com/)
