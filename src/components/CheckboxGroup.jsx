import React, {Component} from 'react';
import Checkbox from './Checkbox';
import PropTypes from 'prop-types';
import classNames from 'classnames';

/** TODO: Implement the following...
 * Add classnames to Checkbox Group, needs to merge with a prop classNames
 * Add SCSS styles to compile with bootstrap style (hide the input)
 * Add Save API prop type? Or just send in with onChange func
 */
class CheckboxGroup extends Component {
    static defaultProps = {
        Component: "div",
        bootstrapStyle: 'true'
    };

    constructor(props) {
        super(props);
        this._onCheckboxChange = this._onCheckboxChange.bind(this);
        this.getValue = this.getValue.bind(this);
        this.state = {
            value: this.props.value || this.props.defaultValue || []
        };
    }

    _renderCheckboxes = (children, maxDepth=1, depth=1) => {
        if (depth > maxDepth) {
            return children;
        }

        const checkboxGroup = {
            name: this.props.name,
            checkedValues: this.state.value,
            onChange: this._onCheckboxChange
        };

        return React.Children.map(children, child => {
            if (!(child && child.$$typeof)) {
                return child;
            }
            else if (child.type === Checkbox) {
                return React.cloneElement(child, {checkboxGroup})
            }
            else {
                return React.cloneElement(child, {}, child.props.children ?
                    React.Children.map(child.props.children, c => this._renderCheckboxes(c, maxDepth, depth + 1))
                    :
                    null
                )
            }
        });
    };

    render() {
        const {
            Component,
            name,
            value,
            onChange,
            children,
            checkboxDepth = 1,
            className,
            bootstrapStyle,
            ...rest
        } = this.props;

        const classnames = classNames({
            "checkboxGroup": true,
            "form-control": bootstrapStyle //TODO: switch this variable based on if bootstrap or material-UI is being used
        });

        return <Component
            className={classnames}
            {...rest}
        >
            {this._renderCheckboxes(children, checkboxDepth)}
        </Component>;
    }

    getValue() {
        return this.state.value;
    }

    _onCheckboxChange(checkboxValue, event) {
        let newValue;
        if (event.target.checked) {
            newValue = this.state.value.concat(checkboxValue);
        } else {
            newValue = this.state.value.filter(v => v !== checkboxValue);
        }

        this.setState({value: newValue});

        if (typeof this.props.onChange === 'function') {
            this.props.onChange(newValue, event, this.props.name);
        }
    }
}
CheckboxGroup.propTypes = {
    Component: PropTypes.elementType,
    name: PropTypes.string,
    value: PropTypes.array,
    onChange: PropTypes.func,
    checkboxDepth: PropTypes.number
};

export default CheckboxGroup;