import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

class Checkbox extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        if (!(this.props && this.props.checkboxGroup)) {
            throw new Error('The `Checkbox` component must be used as a child of `CheckboxGroup`.');
        }
    }

    render() {
        const {
            checkboxGroup: {
                name,
                checkedValues,
                onChange
            },
            ...rest
        } = this.props;

        const optional = {};
        if (checkedValues) {
            optional.checked = (checkedValues.indexOf(this.props.value) >= 0);
        }
        if (typeof onChange === 'function') {
            optional.onChange = onChange.bind(null, this.props.value);
        }

        const classnames = classNames({
            "checkboxGroup__checkbox": true,
            "checkboxGroup__checkbox--checked": optional.checked
        });

        return (
            <span className={classnames}>
                <input
                    {...rest}
                    type="checkbox"
                    name={name}
                    disabled={this.props.disabled}
                    {...optional}
                />
            </span>
        );
    }
}
Checkbox.propTypes = {
    checkboxGroup: PropTypes.object,
};

export default Checkbox;