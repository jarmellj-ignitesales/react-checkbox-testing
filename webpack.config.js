const path = require('path');

const HtmlWebpackPlugin = require("html-webpack-plugin");
const htmlWebpackPlugin = new HtmlWebpackPlugin({
    template: path.join(__dirname, "examples/example.html"),
    filename: "./index.html"
});

module.exports = {
    entry: path.join(__dirname, "examples/example.js"),
    module: {
        rules: [
            {
                test: /\.(js|jsx|mjs)$/,
                use: "babel-loader",
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                use: ["style-loader", "css-loader", "sass-loader"]
            }
        ]
    },
    plugins: [htmlWebpackPlugin],
    resolve: {
        extensions: [".js", ".jsx", ".mjs"]
    },
    devServer: {
        port: 3001
    }
};
